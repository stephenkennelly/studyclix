'use strict';

/**
 * @ngdoc directive
 * @name studyclix2App.directive:menubar
 * @description
 * # menubar
 */
angular.module('studyclix2App')
  .directive('menubar', function () {
    return {
      templateUrl: 'views/partials/menubar.html',
      restrict: 'E',
			scope:{
				back: '@',
				title: '@',
				subjects: '='
			}
			};
  });
