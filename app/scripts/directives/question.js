'use strict';

/**
 * @ngdoc directive
 * @name studyclix2App.directive:question
 * @description
 * # question
 */
angular.module('studyclix2App')
  .directive('question', function () {
    return {
      templateUrl: 'views/partials/question.html',
      restrict: 'E',
			scope:{
				q: '='
			}
    };
  });
