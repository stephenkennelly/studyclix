'use strict';

/**
 * @ngdoc function
 * @name studyclix2App.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the studyclix2App
 */
angular.module('studyclix2App')
  .controller('AboutCtrl', function (dummyData,$scope) {
		$scope.title = "About";
		$scope.back ="#/";
		$scope.user = dummyData.getUser();
  });
