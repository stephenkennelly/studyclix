'use strict';

/**
 * @ngdoc function
 * @name studyclix2App.controller:TopicsCtrl
 * @description
 * # TopicsCtrl
 * Controller of the studyclix2App
 */
angular.module('studyclix2App')
  .controller('TopicsCtrl', function (dummyData , $scope) {

	$scope.title = "Topics";
	$scope.back ="#/";
	$scope.user = dummyData.getUser();
	$scope.question = dummyData.getQuestions()[0];

});
