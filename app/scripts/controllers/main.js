'use strict';

/**
 * @ngdoc function
 * @name studyclix2App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the studyclix2App
 */
angular.module('studyclix2App')
  .controller('MainCtrl', function (dummyData,$scope){
	$scope.title = "Home";
	$scope.back ="#/";
	$scope.user = dummyData.getUser();
  });
