'use strict';

/**
 * @ngdoc service
 * @name studyclix2App.dummyData
 * @description
 * # dummyData
 * Factory in the studyclix2App.
 */
angular.module('studyclix2App')
  .factory('dummyData', function () {
    // Service logic
    // ...

  var user = {
		id:"testUser",
		subjects:["English","Irish","Maths"]
	};
	
	var questions = [
		{
			id : "GeoH2012S1Q9",
			year : "2012",
			section: "1",
			number: "9",
			complete: false,
			markingShown: true,
			imageUrl: "images/q9.png",
			solutionUrl: "images/q9sol.png"
		}
	];
    // Public API here
    return {
      getSubjects: function () {
        return user.subjects;
      },
			getUser: function () {
        return user;
      },
			getQuestions: function(){
				return questions;
			}
    };
  });
